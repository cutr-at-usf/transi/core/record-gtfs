# Record GTFS Job

### Program Type:

	Batch Job

### Execution Frequency:

	Daily

### Program Description

This job fetches a GTFS archive from a transit agency and imports the content if it is not a duplicate. 

The program can be run independently from other services or dependent on the Data and State API Services to retrieve timezone, dataset name and provide centralized logging services.

### Program Execution (API Dependent)

```
docker run --rm --network=my_network \
-e "API_URL=http://basic_api" \
-e "DATASET=hart" \
-v /transi/data/zip/hart:/usr/src/app/zip \
registry.gitlab.com/cutr-at-usf/transi/core/record-gtfs
```

### Execution Parameters

**API_URL**: URL used to access the Data API Service on docker network

**DATASET**: Dataset configuration name as specified in config.json for the Data API service

### Program Execution (Independent Runtime)

```
docker run --rm --network=my_network \
-e "DATA_NAME=HART"  \
-e "TIMEZONE=America/New_York" \
-e "DB_UPLOAD=true"  \
-e "DB_URL=mongodb://db:27017/hart" \
-e "URL_GTFS=http://www.gohart.org/google/google_transit.zip" \
-v /transi/data/zip/hart:/usr/src/app/data \
registry.gitlab.com/cutr-at-usf/transi/core/record-gtfs
```

### Execution Parameters

**DATA_NAME**: Dataset Name. Used as part of the final name of the GTFS archive.

**TIMEZONE**: Used to accurately place PB files into the correct archive.

**DB_UPLOAD**: Boolean value that enables or disables database upload (true/false)

**DB_URL**: URL scheme used to log into a MongoDB database.

**URL_GTFS**: URL for GTFS feed 

**ZIP_PATH** (optional): GTFS directory used within container environment 

Please refer to [getting started project](https://gitlab.com/cutr-at-usf/transi/core/getting-started) for more information about the system
