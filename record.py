from datetime import datetime
import requests
import zipfile
import filecmp
import pytz
import os

GTFS_OUTPUT_DIR = "zip"

config = {}

if os.environ.get('API_URL') is not None:

	parameters = {'source':os.environ.get('DATASET')}

	req = requests.get(os.environ.get('API_URL')+"/status/config/db-url", params=parameters, timeout=10).json()
	config['DB_URL'] = os.environ.get('DB_URL', req['result'])
	req = requests.get(os.environ.get('API_URL')+"/status/config/db-archive", params=parameters, timeout=10).json()
	config['DB_UPLOAD'] = os.environ.get('DB_UPLOAD', req['result'])

	config['ZIP_PATH'] = os.environ.get('ZIP_PATH', GTFS_OUTPUT_DIR)
	req = requests.get(os.environ.get('API_URL')+"/status/config/gtfs-url", params=parameters, timeout=10).json()
	config['URL_GTFS'] = os.environ.get('URL_GTFS', req['result'])
	req = requests.get(os.environ.get('API_URL')+"/status/config/name", params=parameters, timeout=10).json()
	config['DATA_NAME'] = os.environ.get('DATA_NAME', req['result'])
	req = requests.get(os.environ.get('API_URL')+"/status/config/timezone", params=parameters, timeout=10).json()
	config['TIMEZONE'] = os.environ.get('TIMEZONE', req['result'])
else:
	config['DB_URL'] = os.environ.get('DB_URL')
	config['DB_UPLOAD'] = os.environ.get('DB_UPLOAD')
	config['ZIP_PATH'] = os.environ.get('ZIP_PATH', GTFS_OUTPUT_DIR)
	config['URL_GTFS'] = os.environ.get('URL_GTFS')
	config['DATA_NAME'] = os.environ.get('DATA_NAME')
	config['TIMEZONE'] = os.environ.get('TIMEZONE')

tz = pytz.timezone(config['TIMEZONE'])

if os.environ.get('DB_UPLOAD') == 'true':
	import shutil
	import parse
	database_upload = True
else:
	database_upload = False

tmp_filename = "gtfs.zip"

with open(tmp_filename, "wb") as tmp_file:
	print("Downloading "+config['URL_GTFS'])
	response = requests.get(config['URL_GTFS'], stream=True)
	for chunk in response.iter_content(chunk_size=512):
	    if chunk:
	        tmp_file.write(chunk)

if zipfile.is_zipfile(tmp_filename):
	print("Download is Valid")

	latest_file = None
	for file in sorted(os.listdir(config['ZIP_PATH'])):
		if file.endswith(".zip"):
			latest_file = os.path.join(config['ZIP_PATH'], file)

	if latest_file is None:
		proceed = True
	else:
		print("Performing byte-by-byte comparison with "+latest_file)
		if not filecmp.cmp(tmp_filename, latest_file):
			print("Files Different")
			proceed = True
		else: 
			print("Files Same")
			proceed = False

	if proceed:

		out_file = config['DATA_NAME']+"-GTFS-"+datetime.now(tz).strftime("%Y-%m-%d")+".zip"
		if not os.path.isfile(config['ZIP_PATH']+"/"+out_file):

			shutil.copyfile(tmp_filename, config['ZIP_PATH']+"/"+out_file)
			print("Renamed & Copied to "+config['ZIP_PATH']+"/"+out_file)

			if database_upload:
				parse.importGTFS(config['DB_URL'], config['ZIP_PATH']+"/"+out_file, config['TIMEZONE'])
	
	else:
		print("No need to update")
else:
	print("Download is not Valid")