FROM alpine/git as import-gtfs

WORKDIR /usr/src/
RUN git clone https://gitlab.com/cutr-at-usf/transi/core/import-gtfs.git

FROM python:3.5

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY record.py ./
COPY --from=import-gtfs /usr/src/import-gtfs/import.py ./parse.py

CMD [ "python", "-u", "./record.py"]
